# abstrakta

> assisting the human species to perfection

As a social species, we need standards to aid in our communiques;
in order to form a more common understanding of our environment, we construct standard definitions to reach a swift agreement over what something is or is not.

To do well, our standards must be good -- they must be:

 - universal in their understanding
 - exact in their disambiguity
 - obtainable in their definitions
 - extensible in their comprehensiveness, and
 - modular in their implementation.


## implementation

We are an open standards publication group;
the standards are released under the [license](#license) affixed below.

Standards are written in plaintext (utf-8) with AsciiDoctor markup.

The header of a standard is in this format:

    = number.version: name: objective

This is followed by a short rationale.


## contributing

We whole-heartedly welcome contributions to the standards!
Any human may contribute.


### principles

We aim to set standards that would unite and strengthen the human species;
therefore, standards are designed to reflect a "theoretical best" without undue regard to difficultly in implementation.
For this reason, many standards have a <<_practicality>> section intended to help bridge the gap back to reality.

#### open

Our standards must be open to all without cost or regulation.

#### anti-features

We do our best to avoid feature-full waste.

### practicality


### format

Standards are written in the AsciiDoc markup language with semantic newlines.
All link:1.html[numbers] are assumed to be in base ↊r12.


## definitions

standard
:    a specific description detailing a process or entity 

number
:   an arbitrary cardinal number assigned to a standard for ease of reference

version
:   an indication of which particular state the standard was describing

name
:   a tag of the overall intent of a standard

open
:   free to use, modify, redistribute


## license

No entity may be attributed and content must be made freely available to all;
no copyrights, patents, trademarks, or similar restrictions may be made to the material.
these restrictions must be maintained along with any copies or derivatives.
