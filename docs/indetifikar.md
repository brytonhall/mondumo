# identifikar

## uniquely identifying individuals


Identification systems are over-burdened with redundancy and complexity.

In many situations, it is necessary to uniquely (and immutably) identify
someone (or something); currently, nearly every entity has their own, typically
sub-par, method to ensure the identity of an individual. This leads to a single
individual having many different identities generated among many different
sources.


## description

**Immutable Unique Identification** (IUI) is a cryptographic hash of a d-loop
mitochondrial sequence maintained in an peer-to-peer electronic database to
uniquely identify individuals.


## definitions

identification 
  : the immutable public number unique to an individual (obtained through mitochondrial sequencing)
database 
  : any collection of objects associated with a identification
data 
  : any entry in a database
individual
  : a *Homo sapien*
passphrase
  : a private number allotted to an individual to associate themselves to their sequence
authorization
  : a number generated to grant access to specific sets of data.


## Data

 - passphrase
 - records (medical, law)
 - licenses (drivers, machinery, professional)
 - citizenship

## Uses

 - signing documents
 - voting (elections, polls)

### Examples

> An employee gives authorization code to employer to verify their ability to work and drive lawfully.


## Barriers

### Cost
~50 USD per individual?

### Orwellian
Offset by easy of democracy and no gain in shared information.

## Implementation

### Network

Separate databases of identifications with pertinent information. 

### Access Control

Read and write access is determined separately for each set of data and can be granted on a temporary basis.

### Replaces

 - Licenses (driver's, professional)
 - National Identification Numbers
 - Passports/Visas
