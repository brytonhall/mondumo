# formato

standardized digital formats

 - all files encoded in utf-8
 - open formats!


| FORMAT |     TYPE       |       OPEN          | PROPRIETARY
|--------|----------------|---------------------|------------
| text   |                |                     |
|        |  plain         |  txt                |
|        |  rich          |  odf                | doc[x]
|        |  delimited     |  csv,tsv            | xls,xlw
|        |  markup        |  html,tex,xml,json  |
|        |  print-ready   |  pdf,ps             |
|        |  contact       |  caldav             |
|        |  calendar      |  carddav            |
|        |  email         |  maildir,mbox       |
| audio  |                |                     |
|        |  lossless      |  flac               |
|        |  lossy         |  vorbis             | mp3
|        |  music         |  ogg,mgc,speex,opus | wma
| image  |                |                     |
|        |  raster        |  jpeg,png           | tiff,bmp
|        |  vector        |  svg                | wmf
|        |  animated      |                     | gif
|        |  paginated     |                     | pps,ppt
|        |  interactive   |                     | flash


## Contacts

```
<uuid>               must be first entry
<name>               must be second entry
A:<address>
I:<pgp-key>
C:<category>
<email>              <string>@<string>.<string>
<uri>                <scheme>:<string>
<phone>              +<numbers>
<datetime>:<note>
```

entries can be appended with ";<note>"

## Calendars

```
<uuid>
<title>
[
<description>]
<start>|<end>;<interval>
```


## Email

